import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public Title = "Testing";
  public DataFileUrl = 'assets/jsonFiles/test.json';

  constructor(
    private _appService: AppService
  ) {}

  public ngOnInit() {
    this.getData();
  }

  private getData() {
    this._appService.GetTestData(this.DataFileUrl).subscribe((resp) => {
      console.log('Data:', resp);
    })
  }
}
