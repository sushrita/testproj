import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { HttpClient } from '@angular/common/http';
import * as MockData from './../assets/jsonFiles/test.json';

describe('AppService', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    let appService: AppService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                HttpClientTestingModule
            ],
            providers: [
                AppService
            ]
        }).compileComponents();
        httpClient = TestBed.inject(HttpClient);
        httpTestingController = TestBed.inject(HttpTestingController);
        appService = TestBed.inject(AppService);
    }));
    afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
        httpMock.verify();
    }));

    it('should return expected Mock Data by calling once', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.componentInstance;
        appService.GetTestData(app.DataFileUrl).subscribe(
            data => expect(data).toEqual(MockData, 'should return expected data'),
            fail
        );
        const req = httpTestingController.expectOne(app.DataFileUrl);
        expect(req.request.method).toEqual('GET');
        req.flush(MockData);
    });
    
    it('should be OK returning no employee', () => {        
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.componentInstance;
        appService.GetTestData(app.DataFileUrl).subscribe(
            data => expect(data).toEqual({}, 'should have empty object'),
            fail
        );
        const req = httpTestingController.expectOne(app.DataFileUrl);
        req.flush({});
    });
});
