import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class AppService {
    constructor(
        private _http: HttpClient
    ) {}

    public GetTestData(url: string) {
        return this._http.get(url);
    }
}
